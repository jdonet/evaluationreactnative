import React from 'react';
import {View,StyleSheet,Text,ActivityIndicator,NetInfo,ScrollView } from 'react-native';
import {Header, Icon} from 'react-native-elements';
import Login from './components/login';
import CriteresList from './components/criteresList';



var globalURL = "http://evaluation.donet.yt/ws/ws.php?action=";
//var globalURL = "http://192.168.1.62/evaluation/ws/ws.php?action=";
var timer;

export default class App extends React.Component {

  constructor(props){
    super(props);
    this.state ={ 
      isLoading: false,
      logged:false,
      networkPresent: false,
      msgError:"",
      eleveActif:"",
      idConnecte:null,
      eleveConnecte:"",
      criteres:[],
      selectedValues:[]
    }
  }
  /**
   * Check network state
   */
  checkNetWork = () => {
 
  NetInfo.getConnectionInfo().then((connectionInfo) => {
    //console.log('Initial, type: ' + connectionInfo.type + ', effectiveType: ' + connectionInfo.effectiveType);
    if (connectionInfo.type === "none" || connectionInfo.type ==="unknow"){
      this.setState({
        networkPresent: false,
      })
    }else{
      this.setState({
        networkPresent: true,
      })
    }
  });
  function handleFirstConnectivityChange(connectionInfo) {
    //console.log('First change, type: ' + connectionInfo.type + ', effectiveType: ' + connectionInfo.effectiveType);
    NetInfo.removeEventListener(
      'connectionChange',
      handleFirstConnectivityChange
    );
  }
  NetInfo.addEventListener(
    'connectionChange',
    handleFirstConnectivityChange
  );
}


//appelé au lancement de l'application
componentDidMount(){
  this.checkNetWork();
}

getEleveActif = function(){
  return fetch(globalURL.concat('getEleveActif&token=').concat(this.state.token).concat('&id=').concat(this.state.idConnecte))
    .then((response) => response.json())
    .then((responseJson) => {
      //console.log("verif eleve")
      if(this.state.idEleveActif!=responseJson.id || this.state.criteres.length==0){
       // console.log("verif criteres")
        this.getCriteres();
        this.setState({
          idEleveActif:responseJson.id,
          eleveActif: responseJson.nom.concat(" ").concat(responseJson.prenom),
        }, function(){
        });
      }
    })
    .catch((error) =>{
      console.log(error);
    });
}

getCriteres = function(){
  return fetch(globalURL.concat('getCriteres&token=').concat(this.state.token).concat('&id=').concat(this.state.idConnecte))
    .then((response) => response.json())
    .then((responseJson) => {
        this.setState({
          criteres: responseJson.criteres,
          selectedValues:[]
        }, function(){
        });
      
    })
    .catch((error) =>{
      console.log(error);
    });
}

disconnect = () =>{
  clearInterval(timer);
  this.setState({
    logged:false,
    msgError:"",
    eleveActif:"",
    idConnecte:null,
    eleveConnecte:"",
    criteres:[],
    selectedValues:[]
  }, function(){
  });

  
}

onPressLogin= (value) => {
  

  let url = globalURL.concat('login&login=').concat(value.mail).concat('&pass=').concat(value.pass);
  //console.log(url)
  return fetch(url)
  .then((response) => response.json())
  .then((responseJson) => {
    if(responseJson.status === "logged"){
      this.setState({
        token: responseJson.token,
        idConnecte:responseJson.id,
        eleveConnecte:responseJson.nom,
        logged:true
      }, function(){ 
        // Vérification de l'élève actif toutes les 2 secondes
        timer = setInterval(() => this.getEleveActif(), 2000);
      });
    }else{
      console.log('rate');
      this.setState({
        msgError:"Erreur de connexion.\n Réessayez"
        }, function(){
        });
    }
    
  })
  .catch((error) =>{
    console.log(error);
    
    this.setState({
      isLoading: false,
      }, function(){
      });
      
  });
}

onChangePointCallback = (nbPoints, critereId,selectedVal) =>{
  //utilisé pour la sélected value de chaque liste
  this.state.selectedValues[critereId] = selectedVal
  this.forceUpdate()
  /*
  this.setState({
    //selectedValues: update(this.state.selectedValues, {critereId: selectedVal})
  }, function(){ 
  });
*/

  //appel WS pour MAJ la note
  return fetch(globalURL.concat('updateNote&token=').concat(this.state.token).concat('&id=').concat(this.state.idConnecte).concat('&eleve=').concat(this.state.idEleveActif).concat('&critere=').concat(critereId).concat('&points=').concat(nbPoints))
    .then((response) => response.json())
    .then((responseJson) => {
      if(responseJson.status === "ok"){
        console.log("note mise à jour")
      }else{
        console.log('erreur de MAJ de note');
      }
    })
    .catch((error) =>{
      console.log(error);
    });
}
  render(){
    if(this.state.isLoading){
      return(
        <View style={styles.container}>
          <ActivityIndicator/>
          <Text>Chargement en cours ... </Text>
        </View>
      )
    }
    if(this.state.networkPresent){
      if(this.state.logged){
        if(this.state.criteres.length !=0){
          return(
            <View>
              <Header
                outerContainerStyles={styles.header}
                centerComponent={{ text: 'EvalOral', style: { color: '#fff' } }}
                rightComponent={
                  <Icon
                    name='exit-to-app'
                    color='#f50'
                    onPress={() => this.disconnect()} />
                  
                }
              />
              <Text style={styles.titre}>{this.state.eleveActif? this.state.eleveConnecte +" corrige "+ this.state.eleveActif+"\n\n":"Chargement en cours..."}</Text>
              <ScrollView
              contentContainerStyle={styles.scrollV}
              >
                  <CriteresList 
                  criteres={this.state.criteres} 
                  onChangePointCrit = {this.onChangePointCallback}
                  selectedValues = {this.state.selectedValues}
                  />
              </ScrollView>
            </View>
          );
        }else{
          return(
            <View>
              <Header
                outerContainerStyles={styles.header}
                centerComponent={{ text: 'EvalOral', style: { color: '#fff' } }}
                rightComponent={
                  <Icon
                    name='exit-to-app'
                    color='#f50'
                    onPress={() => this.disconnect()} />
                  
                }
              />
              <Text style={styles.titre}>{"Votre enseignant \n a-t-il déjà \n créé une \n interro ?"}</Text>
            </View>
          );
        }
      }else{
        return(
          <Login
          msgError = {this.state.msgError}
          onPressLogin = {this.onPressLogin}
          />
            );
      }
      
    }else{ 
      return(
        <View style={styles.container}>
          <Text>Pas de connexion réseau détectée</Text>
        </View>
      );
      
      
    }
  
  }
}
const styles = StyleSheet.create({
  
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    marginTop:40,
    paddingBottom: 40
  },
  scrollV: {
    paddingBottom: 150
  },
  titre:{ 
    fontSize:20
  },
  containerMsg: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop:20,
  },
  header: {
    backgroundColor: '#555',
  },
});