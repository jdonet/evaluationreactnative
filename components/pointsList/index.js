import React from 'react';
import {Picker} from 'react-native';

import {styles} from './style'

export default class PointsList extends React.Component {
    
    render () {
        console.log
        var pointsItems = [];
        for (var i = 0; i <= this.props.points; i++) {
            pointsItems.push(<Picker.Item label={i.toString()} value={i} key={i} />);
        }
  
      return (
        <Picker
            style={styles.picker}

            onValueChange={(itemValue, itemIndex) => this.props.onChangePoint(itemValue,this.props.critereId,itemIndex)}
            selectedValue={this.props.selectedVal?this.props.selectedVal:0}
            //onValueChange={this.props.onChangePoint}
            key={this.props.clef}
            >
            {pointsItems}
        
        </Picker>
      );
    }
  }


      
