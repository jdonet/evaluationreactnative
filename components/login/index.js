import React from 'react';
import { Text, View,Button } from 'react-native';
import {styles} from './style';
import t from 'tcomb-form-native'; // 0.6.11 
import {Header} from 'react-native-elements';




export default class Login extends React.Component {
  
  onPress= (value) => {
    var value = this.refs.form.getValue();
    if (value) {
      this.props.onPressLogin(value)
    }
  }
  
  render () {
    // Form
    var Form = t.form.Form;

    // Form model
    var User = t.struct({
      mail: t.String,
      pass: t.String,
    });
    var options = {}; // optional rendering options (see documentation)



    return (
      <View> 
        <Header
          outerContainerStyles={styles.header}
          centerComponent={{ text: 'EvalOral', style: { color: '#fff' } }}
        />
        <Text style={styles.titre}>{"\n Connexion de l'élève \n \n"}</Text>

        <Form 
          //ref={c => this._form = c} // assign a ref
          ref="form" // assign a ref
          type={User} 
          options={options}
          />
        <Button
          color="#555"
          title="Connexion"
          onPress={this.onPress}
        />
        <Text>{this.props.msgError}</Text>


      </View>
    );
  }
}


    
