import {StyleSheet} from 'react-native'

export const styles = StyleSheet.create({
  containerLogin: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop:20,
  },
  header: {
    backgroundColor: '#555',
  },
  titre:{ 
    fontSize:20
  },
});