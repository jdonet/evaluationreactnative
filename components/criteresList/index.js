import React from 'react';
import {styles} from './style'
import {View,Text} from 'react-native';
import PointsList from '../pointsList';

const CriteresList = ({criteres,onChangePointCrit,selectedValues}) =>(
    <View> 
        {
            criteres.map((critere, i) => (
              <View key={i}>
                <Text style={styles.critere} key={i}>{critere.nom}</Text>
                <PointsList
                  points={critere.pointMax}
                  onChangePoint={onChangePointCrit} 
                  selectedVal = {selectedValues[critere.id]}
                  //onChangePoint={() => onChangePointCrit()}
                  critereId={critere.id}
                  key={critere.id}
                />
              </View>
            ))
          }
    </View> 
);


export default CriteresList;
      
